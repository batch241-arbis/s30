db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "fruitsOnSale", total: {$sum: 1}}}
]);

db.fruits.aggregate([
	{$match: {stock: {$gt: 20}}},
	{$group: {_id: "enoughStock", total: {$sum: 1}}}
]);

db.fruits.aggregate([
	{$group: {_id:"$supplier_id", avgPricePerSupplier: {$avg:"$price"}}}
])

db.fruits.aggregate([
	{$group: {_id:"$supplier_id", maxPricePerSupplier: {$max:"$price"}}}
])

db.fruits.aggregate([
	{$group: {_id:"$supplier_id", maxPricePerSupplier: {$min:"$price"}}}
])